import numpy
from functools import partial


def vector_mean(vector):
    return sum(vector) / float(len(vector))


def matrix_mean(matrix):
    mean_row = []
    for column in range(matrix.shape[1]):
        mean_row.append(vector_mean(matrix[..., column]))

    return numpy.array([mean_row])


def row_mean_transpose(row, mean):
    return (row - mean) * (row - mean).transpose()


def compute_cov(matrix):
    mean = matrix_mean(matrix)
    fraction = 1 / (float(len(matrix)) - 1)
    cov = fraction * sum(map(partial(row_mean_transpose, mean=mean), matrix))

    return cov


def compute_pca(matrix):
    evals, evecs = numpy.linalg.eig(compute_cov(matrix))

    for i in range(len(evecs)):
        if evecs[:, i][0] < 0:
            evecs[:, i][0] *= -1

    indexing = list(reversed(numpy.argsort(evals)))

    return [evecs[indexing], evals[indexing]]
