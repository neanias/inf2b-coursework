import compute_pca as cpca

import scipy.io
import matplotlib.pyplot as plot


def plot_variance():
    trainf = scipy.io.loadmat('../../svhn')['train_features']
    evals = cpca.compute_pca(trainf)[-1]

    plot.plot(evals.cumsum())
    plot.show()


if __name__ == '__main__':
    plot_variance()
