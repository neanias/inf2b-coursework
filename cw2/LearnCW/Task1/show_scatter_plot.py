import apply_pca as apca

import scipy.io
import matplotlib.pyplot as plot


def plot_scatter():
    x_pca = apca.apply_pca()
    trainc = scipy.io.loadmat('../../svhn')['train_classes']

    plot.scatter(x_pca[:, 0], x_pca[:, 1], c=trainc)
    plot.show()

if __name__ == '__main__':
    plot_scatter()
