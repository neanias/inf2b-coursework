import compute_pca as cpca

import numpy
import scipy.io


def apply_pca():
    x = scipy.io.loadmat('../../svhn')['train_features']
    evecs, evals = cpca.compute_pca(x)

    e_2 = numpy.array((evecs[:, 0], evecs[:, 1])).transpose()

    return numpy.dot(x, e_2)

