import sys
import math
from multiprocessing import Pool
from functools import partial

import numpy
import numpy.linalg
import scipy.io

sys.path.append('../')

import Task1.compute_pca as cpca  # NOQA
import Task2.knn as knn  # NOQA


def gaussian(x, mu, sigma):
    fraction = math.sqrt((2 * numpy.pi)**(len(x)) * numpy.linalg.det(sigma))
    x_mu = x - mu
    power = (-0.5)*(x_mu.dot(numpy.linalg.inv(sigma)).dot(x_mu.transpose()))

    return (1.0/fraction) * math.exp(power)


def generate_entries(features, classes):
    train_pairs = zip(features, classes[0])
    entries = []
    # I know it isn't pretty, but it works...
    for i in xrange(10):  # NOQA
        entries.append([])

    map(lambda t: entries[t[1] - 1].append(t[0]), train_pairs)

    return [numpy.array(entry) for entry in entries]


def gaussian_classify(feature, entries, covs, means):
    probs = [gaussian(feature, means[i], covs[i]) for i in xrange(len(covs))]  # NOQA
    return numpy.argmax(probs) + 1


def classify_full(test, train, classes):
    entries = generate_entries(train, classes)
    covs = map(cpca.compute_cov, entries)
    means = map(cpca.matrix_mean, entries)

    pool = Pool(9)
    group = pool.map(partial(gaussian_classify, entries=entries, covs=covs,
                             means=means),
                     test)
    pool.close()
    pool.join()

    return group


def main():
    data = scipy.io.loadmat('../../svhn')
    trainf = data['train_features']
    trainc = data['train_classes']
    testf = data['test_features']
    testc = data['test_classes']

    classified = classify_full(testf, trainf, trainc)

    confusion = knn.confusion_matrix(classified, testc)
    print(confusion[0])
    print(round(confusion[1], 1))


if __name__ == '__main__':
    main()
