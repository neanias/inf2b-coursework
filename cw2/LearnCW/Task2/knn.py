import numpy
import scipy.io
from multiprocessing import Pool
from functools import partial


def euclidd(a, b):
    return numpy.sqrt(numpy.sum(numpy.square(a - b)))


def find_closest_classes(row, matrix, classes, k):
    distances = []
    for mrow in matrix:
        distances.append(euclidd(row, mrow))

    closest = sorted(zip(distances, classes[0]))[:k]

    return closest


def knn(test, train, train_classes, k):
    # All the threads!
    pool = Pool(9)
    neighbours = pool.map(partial(find_closest_classes, matrix=train,
                                  classes=train_classes, k=k), test)
    pool.close()
    pool.join()

    return [item[1] for sublist in neighbours for item in sublist]


def confusion_matrix(neighbours, test_classes):
    confusion = numpy.zeros((10, 10))
    correct = 0

    for x in xrange(len(neighbours)):  # NOQA
        i = neighbours[x] - 1
        j = test_classes[0][x] - 1
        confusion[i][j] += 1
        if i == j:
            correct += 1

    return (confusion, (float(correct)/len(neighbours))*100)


def main():
    data = scipy.io.loadmat('../../svhn')
    trainf = data['train_features']
    trainc = data['train_classes']
    testf = data['test_features']
    testc = data['test_classes']

    knn_neigh = knn(testf, trainf, trainc, 1)
    print len(knn_neigh)

    confusion = confusion_matrix(knn_neigh, testc)
    print confusion[0]
    print round(confusion[1], 1)

if __name__ == '__main__':
    main()
