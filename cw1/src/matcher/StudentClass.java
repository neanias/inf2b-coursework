package matcher;

public class StudentClass {

    private String text;
    private String pattern;
    private int textLen;
    private int patternLen;
    private int[] prefixFunction;
    private Queue matchIndices;

    public StudentClass(String text, String pattern) {
        this.text = text;
        this.pattern = pattern;
        this.textLen = text.length();
        this.patternLen = pattern.length();
        this.prefixFunction = new int[patternLen + 1];
        this.matchIndices = new Queue();
    }

    public int[] getPrefixFunction() {
        return prefixFunction;
    }

    public void buildPrefixFunction() {
        prefixFunction = ComputePrexFunction(pattern);
    }

    public Queue getMatchIndices() {
        return matchIndices;
    }

    public void search() {
        matchIndices = KMPMatcher(text, pattern);
    }

    public static int[] ComputePrexFunction(String P) {
        int m = P.length();
        // pi array, does Java support unicode variable names...?
        int[] pi = new int[m];
        int k = 0;
        // Given arrays in Java start at 0, q needs to be 1 for the second elem
        for (int q = 1; q < m; q++) {
            // Look at k rather than k + 1 as Java arrays start at 0
            while ((k > 0) && (P.charAt(k) != P.charAt(q))) {
                // Similarly, choose one behind k due to zero-indexed arrays
                k = pi[k - 1];
            }

            if (P.charAt(k) == P.charAt(q)) {
                k++;
            }

            pi[q] = k;
        }

        return pi;
    }

    public static Queue KMPMatcher(String T, String P) {
        int n = T.length();
        int m = P.length();
        int[] pi = ComputePrexFunction(P);
        int q = 0;
        Queue queue = new Queue();

        // If the pattern is longer than the text, return an empty queue
        if (m > n) {
            return queue;
        }

        for (int i = 0; i < n; i++) {
            // As in ComputePrexFunction, check q not q + 1
            while ((q > 0) && (P.charAt(q) != T.charAt(i))) {
                // Again, 1 behind q due to zero-indexed arrays
                q = pi[q - 1];
            }

            if (P.charAt(q) == T.charAt(i)) {
                q++;
            }

            if (q == m) {
                // Look one after (i - m), otherwise you get the char index 
                // before the match
                queue.enqueue(i - m + 1);
                q = pi[q - 1];
            }
        }

        return queue;
    }
}
